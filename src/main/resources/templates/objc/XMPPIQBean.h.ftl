<#ftl ns_prefixes={"D":"http://www.w3.org/2001/XMLSchema"}>
<#import "Utils.ftl" as Utils/>

#import <Foundation/Foundation.h>

#import "MXiBean.h"

@interface ${model.@name} : MXiBean <NSMutableCopying>

<#list model.complexType.sequence.element as attribute>
    <@Utils.renderAttribute attribute=attribute ; name, type>
@property(readwrite, nonatomic<#if Utils.attributeIsSimpleType(attribute) = false >, strong </#if>) ${type} ${name};
    </@Utils.renderAttribute>
</#list>

@end