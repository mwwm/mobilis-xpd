<#ftl ns_prefixes={"D":"http://www.w3.org/2001/XMLSchema"}>
<#import "Utils.ftl" as Utils/>

#import <Foundation/Foundation.h>

#import "DDXML.h"

@interface ${model.@name} : NSObject <NSMutableCopying>

<#list model.sequence.element as attribute>
    <@Utils.renderAttribute attribute=attribute ; name, type>
@property(readwrite, nonatomic<#if Utils.attributeIsSimpleType(attribute) = false >, strong </#if>) ${type} ${name};
    </@Utils.renderAttribute>
</#list>

- (NSXMLElement *)toXML;
- (void)fromXML:(NSXMLElement *)xmlElement;

@end