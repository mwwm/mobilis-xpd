<#function objcTypeForSchemaType schemaType>
	<#if schemaType?starts_with("xs:")>
		<#if schemaType == "xs:string">
			<#return "NSString *"/>
		<#elseif schemaType == "xs:int" || schemaType == "xs:integer">
			<#return "NSNumber *"/>
		<#elseif schemaType == "xs:float" || schemaType == "xs:decimal" || schemaType == "xs:double">
			<#return "NSNumber *"/>
		<#elseif schemaType == "xs:boolean">
			<#return "BOOL"/>
		<#elseif schemaType == "xs:byte">
			<#return "NSData *"/>
		<#elseif ["xs:date", "xs:time", "xs:dateTime", "xs:g"]?seq_contains(schemaType)>
			<#return "NSDate"/>
		</#if>
	<#else>
	<#return "${schemaType} *"/>
	</#if>
</#function>

<#function attributeIsList attribute>
	<#if attribute.@maxOccurs[0]?? && attribute.@maxOccurs?string != "1" >
		<#return true/>
	<#else>
		<#return false/>
	</#if>
</#function>

<#function attributeIsSimpleType attribute>
	<#local schemaType>${attribute.@type}</#local>
	<#if schemaType?starts_with("xs:")>
		<#return true/>
	<#else>
		<#return false/>
	</#if>
</#function>

<#macro paramList params>
	<#list params as param><@renderAttribute attribute=param; name, type>${type} ${name?j_string}</@renderAttribute><#if param_has_next>, </#if></#list><#t/>
</#macro>

<#macro renderAttribute attribute>
<#if attributeIsList(attribute)>
	<#nested attribute.@name "NSMutableArray *">
	<#else>
	<#nested attribute.@name objcTypeForSchemaType(attribute.@type)>
	</#if>
</#macro>