<#ftl ns_prefixes={"D":"http://www.w3.org/2001/XMLSchema"}>
<#import "Utils.ftl" as Utils/>
<#if model["@name"]?cap_first == operation.getMessage().getElement().getLocalPart() >
	<#assign IQ_TYPE="GET">
<#else>
	<#assign IQ_TYPE="RESULT">
</#if>
#import "${model.@name}.h"

#if TARGET_OS_IPHONE
#import "DDXML.h"
#endif

<#list model.complexType.sequence.element as attribute>
<#if Utils.attributeIsList(attribute)>
#import "${attribute.@type}.h"
<#elseif Utils.attributeIsSimpleType(attribute) == false>
#import "${attribute.@type}.h"
</#if>
</#list>

@implementation ${model.@name}

+ (NSString *)elementName
{
    return @"${model.@name}";
}

+ (NSString *)namespace
{
    return @"${protocol.getIdent()}";
}

- (id)mutableCopyWithZone:(NSZone *)zone
{
    ${model.@name} *mutableCopy = [[${model.@name} alloc] init];
    <#list model.complexType.sequence.element as attribute>
    mutableCopy.${attribute.@name} = self.${attribute.@name};
    </#list>

    return mutableCopy;
}

- (id)init
{
    return [self initWithBeanType:${IQ_TYPE} andBeanContainer:BEAN_CONTAINER_MESSAGE];
}

#pragma mark - (De-)Serialization

- (void)fromXML:(NSXMLElement *)xml
{
    <#list model.complexType.sequence.element as attribute>
    <@Utils.renderAttribute attribute=attribute; name, type>
    <#if Utils.attributeIsList(attribute)>
    NSArray *${name}Elements = [xml elementsForName:@"${name}"];
    self.${name} = [[NSMutableArray alloc] initWithCapacity:${name}Elements.count];
    for (NSXMLElement *${name}Element in ${name}Elements)
    {
        <#if Utils.attributeIsSimpleType(attribute)>
            ${Utils.objcTypeForSchemaType(attribute.@type)} element =
            <#if Utils.objcTypeForSchemaType(attribute.@type) == "NSNumber *"> [NSNumber numberWithDouble:[[${name}Element stringValue] doubleValue]];
                <#elseif Utils.objcTypeForSchemaType(attribute.@type) == "NSData *"> [[${name}Element stringValue] dataUsingEncoding:NSUTF8StringEncoding];
                <#elseif Utils.objcTypeForSchemaType(attribute.@type) == "NSString *"> [${name}Element stringValue];
                <#elseif Utils.objcTypeForSchemaType(attribute.@type) == "BOOL"> [NSNumber numberWithBool:[[${name}Element stringValue] boolValue]];
            </#if>
        <#else>
        ${Utils.objcTypeForSchemaType(attribute.@type)} element = [${attribute.@type} new];
        [element fromXML:${name}Element];
        </#if>
        [self.${name} addObject:element];
    }
    <#else>
    <#if Utils.attributeIsSimpleType(attribute)>
    self.${name} =
        <#if Utils.objcTypeForSchemaType(attribute.@type) == "NSNumber *"> [NSNumber numberWithDouble:[[[[xml elementsForName:@"${name}"] firstObject] stringValue] doubleValue]];
        <#elseif Utils.objcTypeForSchemaType(attribute.@type) == "NSData *"> [[[[xml elementsForName:@"${name}"] firstObject] stringValue] dataUsingEncoding:NSUTF8StringEncoding];
        <#elseif Utils.objcTypeForSchemaType(attribute.@type) == "NSString *"> [[[xml elementsForName:@"${name}"] firstObject] stringValue];
        <#elseif Utils.objcTypeForSchemaType(attribute.@type) == "BOOL"> [NSNumber numberWithBool:[[[[xml elementsForName:@"${name}"] firstObject] stringValue] boolValue]];
        </#if>
    <#else>
    self.${name} = [${attribute.@type} new];
    [self.${name} fromXML:[[xml elementsForName:@"${name}"] firstObject]];
    </#if>
    </#if>
    </@Utils.renderAttribute>
    </#list>
}

- (NSXMLElement *)toXML
{
    NSXMLElement *serializedObject = [[NSXMLElement alloc] initWithName:[[self class] elementName] URI:[[self class] namespace]];
    @autoreleasepool {
        <#list model.complexType.sequence.element as attribute>
        <@Utils.renderAttribute attribute=attribute; name, type>
        <#if Utils.attributeIsList(attribute)>
        for (${Utils.objcTypeForSchemaType(attribute.@type)} element in self.${name})
        {
            <#if Utils.attributeIsSimpleType(attribute)>
            NSXMLElement *childElement = [[NSXMLElement alloc] initWithName:@"${name}"];
            [childElement setStringValue:[NSString stringWithFormat:@"%@",element]];
            [serializedObject addChild:childElement];
            <#else>
            [serializedObject addChild:[element toXML]];
            </#if>
        }
        <#else>
            <#if Utils.attributeIsSimpleType(attribute)>
        NSXMLElement *${name}Element = [[NSXMLElement alloc] initWithName:@"${name}"];
        [${name}Element setStringValue:[NSString stringWithFormat:@"%@", self.${name}]];
        [serializedObject addChild:${name}Element];
            <#else>
        [serializedObject addChild:[self.${name} toXML]];
            </#if>
        </#if>
        </@Utils.renderAttribute>
        </#list>
    }
    return serializedObject;
}

@end