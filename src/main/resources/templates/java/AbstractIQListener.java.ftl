<#ftl ns_prefixes={"D":"http://www.w3.org/2001/XMLSchema"}>
<#import "Utils.ftl" as Utils/>
package ${basePackage}.${role?uncap_first};

import de.tudresden.inf.rn.mobilis.xmpp.beans.ProxyBean;
import de.tudresden.inf.rn.mobilis.xmpp.beans.XMPPBean;
import de.tudresden.inf.rn.mobilis.xmpp.mxj.BeanIQAdapter;
import de.tudresden.inf.rn.mobilis.xmpp.mxj.BeanProviderAdapter;

<#assign imports = []>
<#list (incomingIQs+outgoingIQs) as op>
	<#assign imp = op.getIq().getRequest().getElement().getLocalPart()>
	<#if !imports?seq_contains(imp)>
		<#assign imports = imports + [imp]>
	</#if>
	<#assign imp = op.getIq().getResult().getElement().getLocalPart()>
	<#if !imports?seq_contains(imp)>
		<#assign imports = imports + [imp]>
	</#if>
</#list>
<#list imports as imp>
import ${basePackage}.beans.${imp};
</#list>
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Packet;

import java.util.logging.Logger;

public abstract class AbstractIQListener implements PacketListener {

private final static Logger LOGGER = Logger.getLogger(AbstractIQListener.class.getCanonicalName());

	public AbstractIQListener() {
		this.registerBeanPrototypes();
	}
	
	public void registerBeanPrototypes() {
<#list imports as bean>
		(new BeanProviderAdapter(new ProxyBean(${bean}.NAMESPACE, ${bean}.CHILD_ELEMENT))).addToProviderManager();
</#list>
	}

	@Override
	public void processPacket(Packet packet) {
<#if incomingIQs?size &gt; 0 || outgoingIQs?size &gt; 0>
		if (packet instanceof BeanIQAdapter) {
			XMPPBean inBean = ((BeanIQAdapter) packet).getBean();

			LOGGER.info(inBean.toXML());

			if (inBean instanceof ProxyBean) {
				ProxyBean proxyBean = (ProxyBean) inBean;
	<#list incomingIQs as operation>
					<#if operation_index &gt; 0>} else </#if>if (proxyBean.isTypeOf(${operation.getIq().getRequest().getElement().getLocalPart()}.NAMESPACE,
						${operation.getIq().getRequest().getElement().getLocalPart()}.CHILD_ELEMENT)) {
					_on${operation.getIq().getRequest().getElement().getLocalPart()}((${operation.getIq().getRequest().getElement().getLocalPart()}) proxyBean
							.parsePayload(new ${operation.getIq().getRequest().getElement().getLocalPart()}()));
	</#list>
	<#list outgoingIQs as operation>
					<#if incomingIQs?size &gt; 0 || operation_index &gt; 0>} else </#if>if (proxyBean.isTypeOf(${operation.getIq().getResult().getElement().getLocalPart()}.NAMESPACE,
						${operation.getIq().getResult().getElement().getLocalPart()}.CHILD_ELEMENT)) {
					on${operation.getIq().getResult().getElement().getLocalPart()}((${operation.getIq().getResult().getElement().getLocalPart()}) proxyBean
							.parsePayload(new ${operation.getIq().getResult().getElement().getLocalPart()}()));
	</#list>
				} else {
					LOGGER.warning("No responsible type for received proxyBean!");
				}
			}
		}
</#if>
	}

<#list incomingIQs as operation>
	private final void _on${operation.getIq().getRequest().getElement().getLocalPart()}(${operation.getIq().getRequest().getElement().getLocalPart()} inBean) {
		${operation.getIq().getResult().getElement().getLocalPart()} out = new ${operation.getIq().getResult().getElement().getLocalPart()}();
		out.setId(inBean.getId());
		out.setTo(inBean.getFrom());
		out.setFrom(inBean.getTo());
		out.setType(XMPPBean.TYPE_RESULT);
		this.sendIQBean(this.on${operation.getIq().getRequest().getElement().getLocalPart()}(inBean, out));
	}
	
	public abstract ${operation.getIq().getResult().getElement().getLocalPart()} on${operation.getIq().getRequest().getElement().getLocalPart()}(${operation.getIq().getRequest().getElement().getLocalPart()} inBean, ${operation.getIq().getResult().getElement().getLocalPart()} outBean);
	
</#list>
<#list outgoingIQs as operation>
	public abstract void on${operation.getIq().getResult().getElement().getLocalPart()}(${operation.getIq().getResult().getElement().getLocalPart()} inBean);
	
</#list>

	public abstract void sendIQBean(XMPPBean bean);

}