<#function javaTypeForSchemaType schemaType>
	<#if schemaType?starts_with("xs:")>
		<#if schemaType == "xs:string">
			<#return "String"/>
		<#elseif schemaType == "xs:int" || schemaType == "xs:integer">
			<#return "Integer"/>
		<#elseif schemaType == "xs:float">
			<#return "Float"/>
		<#elseif schemaType == "xs:decimal">
			<#return "BigDecimal"/>
		<#elseif schemaType == "xs:double">
			<#return "Double"/>
		<#elseif schemaType == "xs:boolean">
			<#return "Boolean"/>
		<#elseif schemaType == "xs:byte">
			<#return "Byte"/>
		<#elseif ["xs:date", "xs:time", "xs:dateTime", "xs:g"]?seq_contains(schemaType)>
			<#return "javax.xml.datatype.XMLGregorianCalendar"/>
		</#if>
	<#else>
	<#return schemaType/>
	</#if>
</#function>

<#macro paramList params>
	<#list params as param><@renderAttribute attribute=param; name, type>${type} ${name?j_string}</@renderAttribute><#if param_has_next>, </#if></#list><#t/>
</#macro>

<#macro renderAttribute attribute>
	<#if attributeIsList(attribute) >
		<#nested attribute.@name "List<${javaTypeForSchemaType(attribute.@type)}>"> 
	<#else>
		<#nested attribute.@name javaTypeForSchemaType(attribute.@type)>
	</#if>
</#macro>

<#macro parseAttribute attribute>
	<#local schemaType>${attribute.@type}</#local>
	<#if schemaType?starts_with("xs:")>
		<#if schemaType == "xs:string">
			parser.nextText()<#t/>
		<#elseif schemaType == "xs:int" || schemaType == "xs:integer">
			new Integer(parser.nextText())<#t/>
		<#elseif schemaType == "xs:float">
			new Float(parser.nextText())<#t/>
		<#elseif schemaType == "xs:decimal">
			new BigDecimal(parser.nextText())<#t/>
		<#elseif schemaType == "xs:double">
			new Double(parser.nextText())<#t/>
		<#elseif schemaType == "xs:boolean">
			new Boolean(parser.nextText())<#t/>
		<#elseif schemaType == "xs:byte">
			new Byte(parser.nextText())<#t/>
		<#elseif ["xs:date", "xs:time", "xs:dateTime", "xs:g"]?seq_contains(schemaType)>
			new DatatypeFactory().newXMLGregorianCalendar(parser.nextText())<#t/>
		</#if>
	<#else>
		${schemaType}<#t/>
	</#if>
</#macro>

<#macro initializationForAttribute attribute>
	<#local schemaType>${attribute.@type}</#local>
	<#if attributeIsList(attribute)>
		new ArrayList<${javaTypeForSchemaType(attribute.@type)}>()<#t/>
	<#else>
		null<#t/>
	</#if>
</#macro>

<#function attributeIsList attribute>
	<#if attribute.@maxOccurs[0]?? && attribute.@maxOccurs?string != "1" >
		<#return true/> 
	<#else>
		<#return false/>
	</#if>
</#function>

<#function attributeIsSimpleType attribute>
	<#local schemaType>${attribute.@type}</#local>
	<#if schemaType?starts_with("xs:")>
		<#return true/> 
	<#else>
		<#return false/>
	</#if>
</#function>