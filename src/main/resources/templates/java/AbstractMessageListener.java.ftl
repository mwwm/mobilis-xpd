<#ftl ns_prefixes={"D":"http://www.w3.org/2001/XMLSchema"}>
<#import "Utils.ftl" as Utils/>
package ${basePackage}.${role?uncap_first};

import de.tudresden.inf.rn.mobilis.xmpp.beans.XMPPBean;
<#list incomingMessages as operation>
import ${basePackage}.beans.${operation.getMessage().getElement().getLocalPart()};
</#list>
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Message;
import org.xmlpull.mxp1.MXParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.util.logging.Logger;
import java.io.StringReader;

public abstract class AbstractMessageListener implements PacketListener {

private final static Logger LOGGER = Logger.getLogger(AbstractMessageListener.class.getCanonicalName());

	@Override
	public final void processPacket(Packet packet) {
<#if incomingMessages?size &gt; 0>
		if (packet instanceof Message) {
			Message msg = (Message) packet;
			if (null != msg.getBody()) {
				if (msg.getBody().startsWith("<")) {
					XMPPBean msgBean;
					XmlPullParser parser = new MXParser();
					
					try {
						parser.setFeature(MXParser.FEATURE_PROCESS_NAMESPACES, true);
						parser.setInput(new StringReader(msg.getBody()));
	<#list incomingMessages as operation>
						<#if operation_index &gt; 0>} else </#if>if (msg.getBody().startsWith("<"+${operation.getMessage().getElement().getLocalPart()}.CHILD_ELEMENT)) {
							msgBean = new ${operation.getMessage().getElement().getLocalPart()}();
							msgBean.fromXML(parser);
							msgBean.setFrom(msg.getFrom());
							msgBean.setTo(msg.getTo());
							msgBean.setId(msg.getPacketID());
							this.on${operation.getMessage().getElement().getLocalPart()}((${operation.getMessage().getElement().getLocalPart()}) msgBean);
	</#list>
						}
					} catch (XmlPullParserException e) {
						LOGGER.severe(e.getLocalizedMessage());
						return;
					} catch (Exception e) {
						LOGGER.warning("Couldn't parse incoming Message Bean: "+msg.getBody());
						this.handleMessage(msg);
					}
				} else {
					this.handleMessage(msg);
				}
			}
		}
</#if>
	}

<#list incomingMessages as operation>
	public abstract void on${operation.getMessage().getElement().getLocalPart()}(${operation.getMessage().getElement().getLocalPart()} inBean);
	
</#list>
	public void handleMessage(Message msg) {}
}