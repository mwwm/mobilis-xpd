<#ftl ns_prefixes={"D":"http://www.w3.org/2001/XMLSchema"}>
<#import "Utils.ftl" as Utils/>
<#assign attributes=model.complexType.sequence.element/>
package ${basePackage}.beans;

<#if model["count(D:complexType/D:sequence/D:element[@maxOccurs!=1])>0"]>
import java.util.List;
import java.util.ArrayList;
</#if>
import org.xmlpull.v1.XmlPullParser;

import de.tudresden.inf.rn.mobilis.xmpp.beans.XMPPBean;

<#list attributes as attribute>
	<#if !Utils.attributeIsSimpleType(attribute)>
import ${basePackage}.model.${attribute.@type};	
	</#if>
</#list>

public class ${model.@name} extends XMPPBean {

<#list attributes as attribute>
	<@Utils.renderAttribute attribute=attribute ; name, type>
	private ${type} ${name} = <@Utils.initializationForAttribute attribute=attribute/>;
	
	public ${type} get${name?cap_first}() {
		return this.${name};
	}
	
	public void set${name?cap_first}(${type} ${name}) {
		this.${name} = ${name};
	}
	</@Utils.renderAttribute>
</#list>

	public ${model.@name}(<@Utils.paramList params=attributes/>) {
	<#list attributes as attribute>
		this.${attribute.@name} = ${attribute.@name}; 
	</#list>
	}
	
	<#if attributes?size &gt; 0>
	public ${model.@name}() {
	}
	</#if>

	public static final String CHILD_ELEMENT = "${model.@name}";

	@Override
	public String getChildElement() {
		return CHILD_ELEMENT;
	}

	public static final String NAMESPACE = "${protocol.getIdent()}";

	@Override
	public String getNamespace() {
		return NAMESPACE;
	}

	@Override
	public XMPPBean clone() {
	${model.@name} clone = new ${model.@name}();
		this.cloneBasicAttributes(clone);
		return clone;
	}
	
	@Override
	public String payloadToXML() {
		StringBuilder sb = new StringBuilder();
<#list attributes as attribute>
	<@Utils.renderAttribute attribute=attribute; name, type>
		<#if Utils.attributeIsList(attribute)>
		for (${Utils.javaTypeForSchemaType(attribute.@type)} el : this.${name}) {
			sb.append("<${name}>");
			<#if Utils.attributeIsSimpleType(attribute)>
			sb.append(el);
			<#else>
			sb.append(el.payloadToXML());
			</#if>
			sb.append("</${name}>");
		}
		<#else>
		sb.append("<${name}>");
		sb.append(this.${name});
		sb.append("</${name}>");
		</#if>
	</@Utils.renderAttribute>
</#list>
		return sb.toString();
	}

	@Override
	public void fromXML(XmlPullParser parser) throws Exception {
		boolean done = false;
	
		do {
			switch (parser.getEventType()) {
				case XmlPullParser.START_TAG:
					String tagName = parser.getName();
			
					if (tagName.equals(getChildElement())) {
						parser.next();
<#list attributes as attribute>
					} else if (tagName.equals("${attribute.@name}")) {
	<#if !Utils.attributeIsSimpleType(attribute) >
						${attribute.@type} value = new ${attribute.@type}();
						value.fromXML(parser);
	<#else>
						${Utils.javaTypeForSchemaType(attribute.@type)} value = <@Utils.parseAttribute attribute=attribute/>;
	</#if>
	<#if Utils.attributeIsList(attribute)>
						if (null == this.${attribute.@name})
							this.${attribute.@name} = new ArrayList<${Utils.javaTypeForSchemaType(attribute.@type)}>();
						this.${attribute.@name}.add(value);
	<#else>
						this.${attribute.@name} = value;
	</#if>
</#list>
					} else
						parser.next();
				break;
				case XmlPullParser.END_TAG:
					if (parser.getName().equals(getChildElement()))
						done = true;
					else
						parser.next();
				break;
				case XmlPullParser.END_DOCUMENT:
					done = true;
				break;
				default:
					parser.next();
			}
		} while (!done);
	}
}