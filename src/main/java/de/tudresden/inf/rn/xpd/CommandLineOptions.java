package de.tudresden.inf.rn.xpd;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.beust.jcommander.DynamicParameter;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters
public class CommandLineOptions {

	@Parameter(names = { "-xpd", "--protocol-description" }, converter = FileConverter.class, required = true, description = "XPD Protocol Description")
	private File xpdProtocolDescription;

	@Parameter(names = { "-xsd", "--data-scheme" }, converter = FileConverter.class, required = true, description = "XSD Protocol Data Scheme")
	private File xpdDataScheme;

	@Parameter(names = { "-o", "--output-directory" }, converter = DirectoryConverter.class, description = "Folder, where generated files will be stored. Additional structure is generated language dependent.")
	private File outputDirectory = new File(System.getProperty("user.dir"),
			"out/");

	@Parameter(names = { "-l", "--languages" }, variableArity = true, description = "Languages that will be generated. Pass full qualified class names that are available on class path to invoke your custom scripts.")
	private List<String> languages = new ArrayList<>(Arrays.asList("java",
			"objc"));

	@Parameter(names = "--help", help = true, hidden = true)
	private boolean help;

	@DynamicParameter(names = "-D", description = "Provide additional parameters (like package names, etc.) via -D<lang|FQCN>:key=value. These options will be passed to the corresponding compiler language!")
	private Map<String, String> params = new HashMap<String, String>();

	public File getProtocolDescription() {
		return xpdProtocolDescription;
	}

	public File getDataScheme() {
		return xpdDataScheme;
	}

	public File getOutputDirectory() {
		return outputDirectory;
	}

	public List<String> getLanguages() {
		return languages;
	}

	public boolean showHelp() {
		return help;
	}

	public Map<String, String> getParams(String clazz) {
		Map<String, String> result = new HashMap<>();
		for (String string : this.params.keySet()) {
			if (string.split(":")[0].equalsIgnoreCase(clazz)) {
				result.put(string.split(":")[1], this.params.get(string));
			}
		}
		return result;
	}

}
