package de.tudresden.inf.rn.xpd.compiler.languages;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import de.tudresden.inf.rn.xpd.parser.xmlbinding.Operation;
import de.tudresden.inf.rn.xpd.parser.xmlbinding.Protocol;

public class JavaLanguage extends CompilerLanguage {

	private static final Logger LOGGER = LoggerFactory.getLogger(JavaLanguage.class);

	public JavaLanguage(Protocol p, File output, Map<String, String> commandLineParams) {
		super(p, output, commandLineParams);
	}

	@Override
	public List<String> getModelTemplates() {
		List<String> templates = new ArrayList<String>();
		templates.add("XMPPNestedDataModel.java.ftl");
		return templates;
	}

	@Override
	public List<String> getMessageOperationTemplates() {
		List<String> list = new ArrayList<>();
		list.add("XMPPMessageBean.java.ftl");
		return list;
	}

	@Override
	public List<String> getIqOperationTemplates() {
		List<String> list = new ArrayList<>();
		list.add("XMPPIQBean.java.ftl");
		return list;
	}

	@Override
	public String getTemplatePathPrefix() {
		return "/templates/java";
	}

	@Override
	public Map<String, Object> getBaseData() {
		Map<String, Object> baseData = super.getBaseData();
		baseData.put("basePackage", this.getBasePackageName());
		return baseData;
	}

	@Override
	public List<String> getRoleTemplates() {
		List<String> templates = new ArrayList<>();
		templates.add("AbstractIQListener.java.ftl");
		templates.add("AbstractMessageListener.java.ftl");
		return templates;
	}

	@Override
	public Map<String, Object> getDataForRole(QName role) {
		Map<String, Object> data = super.getDataForRole(role);
		return data;
	}

	private String getBasePackageName() {
		if (this.commandLineParams.containsKey("packageNamespace")) {
			return this.commandLineParams.get("packageNamespace");
		} else {
			return this.getBasePackageNameFromServiceURI(this.protocol.getIdent());
		}
	}

	private String getBasePackageNameFromServiceURI(String ident) {
		StringBuilder packageNameSpace = new StringBuilder();
		String url = ident.split("://")[1];
		String domain = url.split("/")[0];
		String[] domainParts = domain.split("\\.");
		for (int i = domainParts.length - 1; i >= 0; i--) {
			if (!domainParts[i].startsWith("www")) {
				packageNameSpace.append(normalizeString(domainParts[i])).append(".");
			}
		}
		String[] pathComponents = url.split("/");
		for (int i = 1, j = pathComponents.length; i < j; i++) {
			if (i == j - 1 && pathComponents[i].contains(".")) {
				packageNameSpace.append(normalizeString(pathComponents[i].split(".")[0]));
			} else if (i == j - 1) {
				packageNameSpace.append(normalizeString(pathComponents[i]));
			} else {
				packageNameSpace.append(normalizeString(pathComponents[i])).append(".");
			}
		}

		return packageNameSpace.toString();
	}

	private String normalizeString(String s) {
		StringBuilder sb = new StringBuilder();
		if (!Character.isJavaIdentifierStart(s.charAt(0))) {
			sb.append("_");
		}
		for (char c : s.toCharArray()) {
			if (!Character.isJavaIdentifierPart(c)) {
				sb.append("_");
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}

	@Override
	public Writer getWriterForMessageTemplate(Operation op, String t) {
		StringBuilder path = new StringBuilder(this.output.getAbsolutePath());
		path.append(File.separator);
		path.append(getBasePackageName().replace(".", File.separator));
		path.append(File.separator);
		path.append("beans");
		path.append(File.separator);
		path.append(op.getMessage().getElement().getLocalPart());
		path.append(".java");

		return createFileWriterForPath(path.toString());
	}

	@Override
	public Writer getWriterForIqRequestTemplate(Operation op, String t) {
		StringBuilder path = new StringBuilder(this.output.getAbsolutePath());
		path.append(File.separator);
		path.append(getBasePackageName().replace(".", File.separator));
		path.append(File.separator);
		path.append("beans");
		path.append(File.separator);
		path.append(op.getIq().getRequest().getElement().getLocalPart());
		path.append(".java");

		return createFileWriterForPath(path.toString());
	}

	@Override
	public Writer getWriterForIqResultTemplate(Operation op, String t) {
		StringBuilder path = new StringBuilder(this.output.getAbsolutePath());
		path.append(File.separator);
		path.append(getBasePackageName().replace(".", File.separator));
		path.append(File.separator);
		path.append("beans");
		path.append(File.separator);
		path.append(op.getIq().getResult().getElement().getLocalPart());
		path.append(".java");

		return createFileWriterForPath(path.toString());
	}

	@Override
	public Writer getWriterForModelTemplate(Element model, String t) {
		StringBuilder path = new StringBuilder(this.output.getAbsolutePath());
		path.append(File.separator);
		path.append(getBasePackageName().replace(".", File.separator));
		path.append(File.separator);
		path.append("model");
		path.append(File.separator);
		path.append(model.getAttribute("name"));
		path.append(".java");

		return createFileWriterForPath(path.toString());
	}

	@Override
	public Writer getWriterForRoleTemplate(QName role, String t) {
		StringBuilder path = new StringBuilder(this.output.getAbsolutePath());
		path.append(File.separator);
		path.append(getBasePackageName().replace(".", File.separator));
		path.append(File.separator);
		path.append(this.firstCharacterDown(role.getLocalPart()));
		path.append(File.separator);
		path.append(t.split("\\.")[0]);
		path.append(".java");

		return createFileWriterForPath(path.toString());
	}

	private final Writer createFileWriterForPath(String path) {
		try {
			File f = new File(path);
			f.getParentFile().mkdirs();
			return new FileWriter(f);
		} catch (IOException e) {
			LOGGER.error(e.getLocalizedMessage(), e);
		}
		return CONSOLE_WRITER;
	}

    private String firstCharacterDown(final String input)
    {
        return Character.toLowerCase(input.charAt(0)) + (input.length() > 1 ? input.substring(1) : "");
    }
}
