package de.tudresden.inf.rn.xpd.compiler.languages;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.tudresden.inf.rn.xpd.parser.xmlbinding.Operation;
import de.tudresden.inf.rn.xpd.parser.xmlbinding.Protocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import javax.xml.namespace.QName;

/**
 * @author cmdaltent
 */
public class ObjCLanguage extends CompilerLanguage {

    private static final Logger LOGGER = LoggerFactory.getLogger(ObjCLanguage.class);

    public ObjCLanguage(Protocol p, File output, Map<String, String> commandLineParams) {
		super(p, output, commandLineParams);
	}

	@Override
	public List<String> getMessageOperationTemplates() {
		List<String> list = new ArrayList<>(2);
		list.add("XMPPMessageBean.h.ftl");
		list.add("XMPPMessageBean.m.ftl");
		return list;
	}

	@Override
	public List<String> getIqOperationTemplates() {
		List<String> list = new ArrayList<>(2);
		list.add("XMPPIQBean.h.ftl");
		list.add("XMPPIQBean.m.ftl");
		return list;
	}

	@Override
	public List<String> getModelTemplates() {
		List<String> list = new ArrayList<>(2);
		list.add("XMPPNestedDataModel.h.ftl");
		list.add("XMPPNestedDataModel.m.ftl");
		return list;
	}

	@Override
	public String getTemplatePathPrefix() {
		return "/templates/objc";
	}

	@Override
	public List<String> getRoleTemplates() {
		List<String> templates = new ArrayList<>();
		return templates;
	}

    @Override
    public Map<String, Object> getDataForRole(QName role)
    {
        return super.getDataForRole(role);
    }

    @Override
    public Writer getWriterForMessageTemplate(Operation op, String t)
    {
        StringBuilder path = new StringBuilder(this.output.getAbsolutePath());
        path.append(File.separator);
        path.append(this.getBaseOutputDir());
        path.append(File.separator);
        path.append("Messages");
        path.append(File.separator);
        path.append(op.getMessage().getElement().getLocalPart());
        path.append(".").append(t.split("\\.")[1]);

        return this.createFileWriterForPath(path.toString());
    }

    @Override
    public Writer getWriterForIqRequestTemplate(Operation op, String t)
    {
        StringBuilder path = new StringBuilder(this.output.getAbsolutePath());
        path.append(File.separator);
        path.append(this.getBaseOutputDir());
        path.append(File.separator);
        path.append("IQs");
        path.append(File.separator);
        path.append(op.getIq().getRequest().getElement().getLocalPart());
        path.append(".").append(t.split("\\.")[1]);

        return this.createFileWriterForPath(path.toString());
    }

    @Override
    public Writer getWriterForIqResultTemplate(Operation op, String t)
    {
        StringBuilder path = new StringBuilder(this.output.getAbsolutePath());
        path.append(File.separator);
        path.append(this.getBaseOutputDir());
        path.append(File.separator);
        path.append("IQs");
        path.append(File.separator);
        path.append(op.getIq().getResult().getElement().getLocalPart());
        path.append(".").append(t.split("\\.")[1]);

        return this.createFileWriterForPath(path.toString());
    }

    public Writer getWriterForModelTemplate(Element model, String t)
    {
        StringBuilder path = new StringBuilder(this.output.getAbsolutePath());
        path.append(File.separator);
        path.append(this.getBaseOutputDir());
        path.append(File.separator);
        path.append("Model");
        path.append(File.separator);
        path.append(model.getAttribute("name"));
        path.append(".").append(t.split("\\.")[1]);

        return this.createFileWriterForPath(path.toString());
    }

    public Writer getWriterForRoleTemplate(QName role, String t)
    {
        StringBuilder path = new StringBuilder(this.output.getAbsolutePath());
        path.append(File.separator);
        path.append(this.getBaseOutputDir());
        path.append(File.separator);
        path.append(role.getLocalPart());
        path.append(t.split("\\.")[0]);
        path.append(".").append(t.split("\\.")[1]);

        return this.createFileWriterForPath(path.toString());
    }

    private Writer createFileWriterForPath(final String path)
    {
        try {
            File f = new File(path);
            f.getParentFile().mkdirs();
            return new FileWriter(f);
        } catch (IOException e) {
            LOGGER.error(e.getLocalizedMessage(), e);
        }

        return CONSOLE_WRITER;
    }

    private String getBaseOutputDir()
    {
        return "Classes";
    }
}
