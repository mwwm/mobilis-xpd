package de.tudresden.inf.rn.xpd;

import java.io.File;

import com.beust.jcommander.IStringConverter;

public class DirectoryConverter implements IStringConverter<File> {

	public DirectoryConverter() {
	}

	@Override
	public File convert(String value) {
		File f;
		if (value.startsWith("/")) {
			f =  new File(value);
		} else {
			f = new File(System.getProperty("user.dir"), value);
		}
		if (!f.exists() || !f.isDirectory()) {
			if (!f.mkdirs()) {
				throw new Error("Directory "+value+" doesn't exist and couldn't be created!");
			}
		}
		return f;
	}
}
