package de.tudresden.inf.rn.xpd;

import java.io.File;

import com.beust.jcommander.IStringConverter;

public class FileConverter implements IStringConverter<File> {

	public FileConverter() {
	}

	@Override
	public File convert(String file) {
		File f;
		if (file.startsWith("/")) {
			f =  new File(file);
		} else {
			f = new File(System.getProperty("user.dir"), file);
		}
		return f;
	}
}
