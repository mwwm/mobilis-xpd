package de.tudresden.inf.rn.xpd.compiler.languages;

import java.io.File;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.w3c.dom.Element;

import de.tudresden.inf.rn.xpd.CommandLineOptions;
import de.tudresden.inf.rn.xpd.parser.xmlbinding.Operation;
import de.tudresden.inf.rn.xpd.parser.xmlbinding.Protocol;
import de.tudresden.inf.rn.xpd.parser.xmlbinding.Request;
import de.tudresden.inf.rn.xpd.parser.xmlbinding.Result;
import freemarker.template.Configuration;

public abstract class CompilerLanguage {

	/**
	 * Protocol part of XPD.
	 */
	protected final Protocol protocol;

	/**
	 * The output directory (./out is default)
	 */
	protected final File output;

	/**
	 * Dynamic command line parameters for the concrete language.
	 */
	protected final Map<String, String> commandLineParams;

	/**
	 * Abstract class {@link CompilerLanguage} represents a programming language
	 * that should be generated from XPD & XSD. Some methods need to be
	 * overridden, some can be overridden.
	 * 
	 * @param protocol {@link Protocol} that is provided by the XPD file.
	 * @param output Directory to put generated files into.
	 * @param commandLineParams Additional parameters that are provided by
	 *        command line interface. Pass command line parameters via
	 *        -D&lt;java|objc|full qualified class
	 *        name&gt;:&lt;key&gt;=&lt;value&gt;. They are automatically parsed
	 *        and passed as key-value map to the corresponding language
	 *        implementation. Class prefixes are removed before.
	 */
	public CompilerLanguage(Protocol protocol, File output, Map<String, String> commandLineParams) {
		this.protocol = protocol;
		this.output = output;
		this.commandLineParams = commandLineParams;
	}

	/**
	 * Default Writer, intended for debugging and testing.
	 */
	protected static final Writer CONSOLE_WRITER = new OutputStreamWriter(System.out);

	/**
	 * Path to templates, relative to concrete {@link CompilerLanguage} class'
	 * resources directory. Will be used for
	 * {@link Configuration#setClassForTemplateLoading(Class, String)} where
	 * Class is the concrete {@link CompilerLanguage} class and String the
	 * return value of this method.
	 * 
	 * @return {@link String} path to templates
	 */
	public String getTemplatePathPrefix() {
		return "/templates";
	}

	/**
	 * Return a list of template filenames that are located in the directory
	 * defined by @{@link #getTemplatePathPrefix()}.
	 * 
	 * @return list of template file names
	 */
	public abstract List<String> getMessageOperationTemplates();

	/**
	 * Return a list of template filenames that are located in the directory
	 * defined by @{@link #getTemplatePathPrefix()}. These templates are
	 * executed once per IQ Operation. However, it doesn't differentiate between
	 * Requests and Results.
	 * 
	 * @return list of template file names
	 */
	public abstract List<String> getIqOperationTemplates();

	/**
	 * provide additional data for each message operation to templates.
	 * Additional information for templates and data structure is available at
	 * {@linkplain http://freemarker.org/docs/index.html}
	 * 
	 * @param op {@link Operation}
	 * @return Map containing key-value binding for template parameters
	 */
	public Map<String, Object> getDataForMessageOperation(Operation op) {
		return new HashMap<String, Object>();
	}

	/**
	 * provide additional data for each IQ based operation to templates.
	 * Additional information for templates and data structure is available at
	 * {@linkplain http://freemarker.org/docs/index.html}
	 * 
	 * @param op {@link Operation}
	 * @return Map containing key-value binding for template parameters
	 */
	public Map<String, Object> getDataForIqOperation(Operation op) {
		return new HashMap<String, Object>();
	}

	/**
	 * provide additional data for each request of an IQ based operation to
	 * templates. Additional information for templates and data structure is
	 * available at {@linkplain http://freemarker.org/docs/index.html}
	 * 
	 * @param request {@link Request}
	 * @return Map containing key-value binding for template parameters
	 */
	public Map<String, Object> getDataForIqRequest(Request request) {
		return new HashMap<String, Object>();
	}

	/**
	 * provide additional data for each result of an IQ based operation to
	 * templates. Additional information for templates and data structure is
	 * available at {@linkplain http://freemarker.org/docs/index.html}
	 * 
	 * @param result {@link Result}
	 * @return Map containing key-value binding for template parameters
	 */
	public Map<String, Object> getDataForIqResult(Result result) {
		return new HashMap<String, Object>();
	}

	/**
	 * provide additional data for all templates. Additional information for
	 * templates and data structure is available at {@linkplain http
	 * ://freemarker.org/docs/index.html}
	 * 
	 * @return Map containing key-value binding for template parameters
	 */
	public Map<String, Object> getBaseData() {
		return new HashMap<String, Object>();
	}

	/**
	 * provide additional data for each data model to templates. Additional
	 * information for templates and data structure is available at
	 * {@linkplain http://freemarker.org/docs/index.html}
	 * 
	 * @param o {@link Element}
	 * @return Map containing key-value binding for template parameters
	 */
	public Map<String, Object> getDataForModel(Element o) {
		return new HashMap<String, Object>();
	}

	/**
	 * Return a list of template filenames that are located in the directory
	 * defined by @{@link #getTemplatePathPrefix()}. These templates are
	 * executed once per xs:complexType defined in an XSD file. This is intended
	 * to be a nested data model which is reused in serveral operations.
	 * 
	 * @return list of template file names
	 */
	public abstract List<String> getModelTemplates();

	/**
	 * Return a {@link Writer} for a message template to write generated sources
	 * to disk.
	 * 
	 * @param op corresponding {@link Operation}
	 * @param t corresponding template file name, might be needed for file
	 *        extensions
	 * @return a {@link Writer}, defaults to System.out
	 */
	public Writer getWriterForMessageTemplate(Operation op, String t) {
		return CONSOLE_WRITER;
	}

	/**
	 * Return a {@link Writer} for an IQ based request template to write
	 * generated sources to disk.
	 * 
	 * @param op corresponding {@link Operation}
	 * @param t corresponding template file name, might be needed for file
	 *        extensions
	 * @return a {@link Writer}, defaults to System.out
	 */
	public Writer getWriterForIqRequestTemplate(Operation op, String t) {
		return CONSOLE_WRITER;
	}

	/**
	 * Return a {@link Writer} for an IQ based result template to write
	 * generated sources to disk.
	 * 
	 * @param op corresponding {@link Operation}
	 * @param t corresponding template file name, might be needed for file
	 *        extensions
	 * @return a {@link Writer}, defaults to System.out
	 */
	public Writer getWriterForIqResultTemplate(Operation op, String t) {
		return CONSOLE_WRITER;
	}

	/**
	 * Return a {@link Writer} for a data model template to write generated
	 * sources to disk.
	 * 
	 * @param model corresponding {@link Element} of XSD file
	 * @param t corresponding template file name, might be needed for file
	 *        extensions
	 * @return a {@link Writer}, defaults to System.out
	 */
	public Writer getWriterForModelTemplate(Element model, String t) {
		return CONSOLE_WRITER;
	}

	/**
	 * Return a list of template filenames that are located in the directory
	 * defined by @{@link #getTemplatePathPrefix()}. These templates are
	 * executed once per defined role and should create some logic (handling
	 * bean registration, proxy stuff etc.)
	 * 
	 * @return list of template file names
	 */
	public abstract List<String> getRoleTemplates();

	/**
	 * provide additional data for role templates. Additional information for
	 * templates and data structure is available at {@linkplain http
	 * ://freemarker.org/docs/index.html}
	 * 
	 * @param role {@link QName}
	 * @return Map containing key-value binding for template parameters
	 */
	public Map<String, Object> getDataForRole(QName role) {
		return new HashMap<String, Object>();
	}

	/**
	 * Return a {@link Writer} for role templates to write generated sources to
	 * disk.
	 * 
	 * @param role corresponding {@link QName} role. Usually
	 *        {@link QName#getLocalPart()} is used to differentiate packages or
	 *        namespaces.
	 * @param t corresponding template file name, might be needed for file
	 *        extensions. Usually the first part of the template name is used as
	 *        file name.
	 * @return a {@link Writer}, defaults to System.out
	 */
	public Writer getWriterForRoleTemplate(QName role, String t) {
		return CONSOLE_WRITER;
	}

	/**
	 * Returns the output directory passed via command line options. Uses ./out
	 * as default. The applications tries to create the directory if it doesn't
	 * exist.
	 * 
	 * @return {@link File} output directory
	 */
	public File getOutputDirectory() {
		return output;
	}

	/**
	 * Returns the dynamic command line parameters that were already filtered by {@link CommandLineOptions}.
	 * @return {@link Map} containing only the keys and their values.
	 */
	public Map<String, String> getCommandLineParams() {
		return commandLineParams;
	}
}
