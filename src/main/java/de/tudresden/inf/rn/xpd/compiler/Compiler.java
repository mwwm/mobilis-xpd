package de.tudresden.inf.rn.xpd.compiler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import de.tudresden.inf.rn.xpd.compiler.languages.CompilerLanguage;
import de.tudresden.inf.rn.xpd.parser.xmlbinding.Operation;
import de.tudresden.inf.rn.xpd.parser.xmlbinding.Operation.Direction;
import de.tudresden.inf.rn.xpd.parser.xmlbinding.Protocol;
import de.tudresden.inf.rn.xpd.parser.xmlbinding.Xpd;
import freemarker.ext.dom.NodeModel;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public final class Compiler {

	private static final Logger logger = LoggerFactory.getLogger(Compiler.class);

	private static final NamespaceContext NAMESPACE_CONTEXT = new NamespaceContext() {

		@Override
		public Iterator getPrefixes(String namespaceURI) {
			return null;
		}

		@Override
		public String getPrefix(String namespaceURI) {
			if ("http://www.w3.org/2001/XMLSchema".equalsIgnoreCase(namespaceURI)) {
				return "xs";
			}
			return null;
		}

		@Override
		public String getNamespaceURI(String prefix) {
			if ("xs".equalsIgnoreCase(prefix)) {
				return "http://www.w3.org/2001/XMLSchema";
			}
			return null;
		}
	};

	private final Protocol protocol;
	private final List<Operation> operations;
	private final Document model;
	private CompilerLanguage language = null;

	private Configuration cfg;
	private XPathFactory xPathFactory = XPathFactory.newInstance();

	public Compiler(Xpd protocol, Document model) {
		this.protocol = protocol.getProtocol();
		this.operations = protocol.getOperation();
		this.model = model;
		this.cfg = new Configuration();
	}

	public void compile(CompilerLanguage lang) {
		this.language = lang;
		cfg.setClassForTemplateLoading(lang.getClass(), lang.getTemplatePathPrefix());

		this.compileDataModel();

		for (Operation op : this.operations) {
			if (null != op.getMessage()) {
				this.compileMessageOperation(op);
			} else if (null != op.getIq()) {
				this.compileIqOperation(op);
			} else if (null != op.getPresence()) {
				this.compilePresenceOperation(op);
			} else
				logger.warn(op.toString());
		}

		for (QName role : this.protocol.getRole()) {
			this.compileRole(role);
		}
	}

	private void compileRole(QName role) {
		List<String> roleTemplates = language.getRoleTemplates();
		if (null == roleTemplates || roleTemplates.size() == 0) {
			logger.warn("No Role Templates available!");
			return;
		}
		Map<String, Object> baseDataSource = new HashMap<String, Object>();
		baseDataSource.putAll(language.getBaseData());
		baseDataSource.putAll(language.getDataForRole(role));
		baseDataSource.put("protocol", this.protocol);
		baseDataSource.put("role", role.getLocalPart());
		Set<Operation> incomingIQs = new HashSet<>(this.operations.size());
		Set<Operation> outgoingIQs = new HashSet<>(this.operations.size());
		for (Operation op : this.operations) {
			if (null == op.getIq())
				continue;
			for (Direction dir : op.getDirection()) {
				if (dir.getFrom().equals(role)) {
					outgoingIQs.add(op);
				} else if (dir.getTo().equals(role)) {
					incomingIQs.add(op);
				}
			}
		}
		baseDataSource.put("incomingIQs", incomingIQs);
		baseDataSource.put("outgoingIQs", outgoingIQs);

		Set<Operation> incomingMessages = new HashSet<>(this.operations.size());
		Set<Operation> outgoingMessages = new HashSet<>(this.operations.size());
		for (Operation op : this.operations) {
			if (null == op.getMessage())
				continue;
			for (Direction dir : op.getDirection()) {
				if (dir.getFrom().equals(role)) {
					outgoingMessages.add(op);
				} else if (dir.getTo().equals(role)) {
					incomingMessages.add(op);
				}
			}
		}
		baseDataSource.put("incomingMessages", incomingMessages);
		baseDataSource.put("outgoingMessages", outgoingMessages);

		
		for (String t : roleTemplates) {
			this.processTemplate(t, baseDataSource, this.language.getWriterForRoleTemplate(role, t));
		}

	}

	private void compileDataModel() {
		NodeList types = null;
		try {
			XPath xPath = xPathFactory.newXPath();
			xPath.setNamespaceContext(NAMESPACE_CONTEXT);
			types = (NodeList) xPath.evaluate("/xs:schema/xs:complexType",
					model.getDocumentElement(), XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			logger.error(e.getLocalizedMessage(), e);
		}

		if (types.getLength() == 0)
			return;

		List<String> modelTemplates = language.getModelTemplates();
		if (null == modelTemplates || modelTemplates.size() == 0) {
			logger.error("Found model in XSD but no templates for data models available!");
			return;
		}
		Map<String, Object> baseDataSource = new HashMap<String, Object>();
		baseDataSource.putAll(language.getBaseData());
		baseDataSource.put("protocol", this.protocol);

		for (int i = 0, j = types.getLength(); i < j; i++) {
			Map<String, Object> dataSourceForModel = new HashMap<>(baseDataSource);
			dataSourceForModel.putAll(language.getDataForModel((Element) types.item(i)));
			dataSourceForModel.put("model", NodeModel.wrap((Element) types.item(i)));

			for (String t : modelTemplates) {
				this.processTemplate(t, dataSourceForModel,
						this.language.getWriterForModelTemplate((Element) types.item(i), t));
			}
		}
	}

	private void compilePresenceOperation(Operation op) {
	}

	private void compileIqOperation(Operation op) {
		List<String> iqTemplates = language.getIqOperationTemplates();
		if (null == iqTemplates || iqTemplates.size() == 0) {
			logger.error("IQ Operationes defined in XPD but no templates for IQs available!");
			return;
		}
		Map<String, Object> dataSource = new HashMap<String, Object>();
		dataSource.putAll(language.getBaseData());
		dataSource.putAll(language.getDataForIqOperation(op));
		dataSource.put("protocol", this.protocol);
		dataSource.put("operation", op);

		Map<String, Object> dataSourceForRequest = new HashMap<>(dataSource);
		dataSourceForRequest.putAll(language.getDataForIqRequest(op.getIq().getRequest()));
		Element o = null;
		try {
			XPath xPath = xPathFactory.newXPath();
			xPath.setNamespaceContext(NAMESPACE_CONTEXT);
			o = (Element) xPath.evaluate(
					"/xs:schema/xs:element[@name='" + op.getIq().getRequest().getElement().getLocalPart() + "']",
					model.getDocumentElement(), XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			logger.error(e.getLocalizedMessage(), e);
		}
		dataSourceForRequest.put("model", NodeModel.wrap(o));

		for (String t : iqTemplates) {
			processTemplate(t, dataSourceForRequest, this.language.getWriterForIqRequestTemplate(op, t));
		}

		Map<String, Object> dataSourceForResult = new HashMap<>(dataSource);
		dataSourceForRequest.putAll(language.getDataForIqResult(op.getIq().getResult()));
		o = null;
		try {
			XPath xPath = xPathFactory.newXPath();
			xPath.setNamespaceContext(NAMESPACE_CONTEXT);
			o = (Element) xPath.evaluate(
					"/xs:schema/xs:element[@name='" + op.getIq().getResult().getElement().getLocalPart() + "']",
					model.getDocumentElement(), XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			logger.error(e.getLocalizedMessage(), e);
		}
		dataSourceForResult.put("model", NodeModel.wrap(o));

		for (String t : iqTemplates) {
			this.processTemplate(t, dataSourceForResult, this.language.getWriterForIqResultTemplate(op, t));
		}
	}

	private void compileMessageOperation(Operation op) {
		List<String> messageTemplates = language.getMessageOperationTemplates();
		if (null == messageTemplates || messageTemplates.size() == 0) {
			logger.error("IQ Operationes defined in XPD but no templates for IQs available!");
			return;
		}
		Map<String, Object> dataSource = new HashMap<String, Object>();
		dataSource.putAll(language.getBaseData());
		dataSource.putAll(language.getDataForMessageOperation(op));

		dataSource.put("protocol", this.protocol);
		Element o = null;
		try {
			XPath xPath = xPathFactory.newXPath();
			xPath.setNamespaceContext(NAMESPACE_CONTEXT);
			o = (Element) xPath.evaluate(
					"/xs:schema/xs:element[@name='" + op.getMessage().getElement().getLocalPart() + "']",
					model.getDocumentElement(), XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			logger.error(e.getLocalizedMessage(), e);
		}
		dataSource.put("model", NodeModel.wrap(o));
		dataSource.put("operation", op);

		for (String t : messageTemplates) {
			this.processTemplate(t, dataSource, this.language.getWriterForMessageTemplate(op, t));
		}

	}

	private void processTemplate(String t, Map<String, Object> data, Writer out) {
		Template tpl;
		try {
			tpl = cfg.getTemplate(t);
			tpl.process(data, out);
		} catch (FileNotFoundException e) {
			logger.error(e.getLocalizedMessage(), e);
		} catch (IOException e) {
			logger.error(e.getLocalizedMessage(), e);
		} catch (TemplateException e) {
			logger.error(e.getFTLInstructionStack(), e);
		}
	}

}
