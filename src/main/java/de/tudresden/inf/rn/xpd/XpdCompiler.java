package de.tudresden.inf.rn.xpd;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import javax.xml.bind.JAXB;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

import de.tudresden.inf.rn.xpd.compiler.Compiler;
import de.tudresden.inf.rn.xpd.compiler.languages.CompilerLanguage;
import de.tudresden.inf.rn.xpd.compiler.languages.JavaLanguage;
import de.tudresden.inf.rn.xpd.compiler.languages.ObjCLanguage;
import de.tudresden.inf.rn.xpd.parser.xmlbinding.Protocol;
import de.tudresden.inf.rn.xpd.parser.xmlbinding.Xpd;
import freemarker.ext.dom.NodeModel;

/**
 * Hello world!
 * 
 */
public class XpdCompiler {

	private final static Logger logger = LoggerFactory.getLogger(XpdCompiler.class);

	public static void main(String[] args) {
		CommandLineOptions clo = new CommandLineOptions();
		JCommander cmd = new JCommander(clo);
		if (clo.showHelp()) {
			cmd.usage();
			System.exit(0);
		}
		try {
			cmd.parse(args);
		} catch (ParameterException e) {
			cmd.usage();
			System.exit(1);
		}

		try {
			freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_SLF4J);
		} catch (ClassNotFoundException e1) {
			logger.error(e1.getLocalizedMessage(), e1);
		}
		Xpd xpd = JAXB.unmarshal(clo.getProtocolDescription(), Xpd.class);

		NodeModel nm = null;
		Compiler compiler;
		try {
			nm = NodeModel.parse(clo.getDataScheme());

			compiler = new Compiler(xpd, (Document) nm.getNode());
			for (String string : clo.getLanguages()) {
				if (string.equalsIgnoreCase("objc")) {
					compiler.compile(new ObjCLanguage(xpd.getProtocol(), clo.getOutputDirectory(), clo.getParams("objc")));
				} else if (string.equalsIgnoreCase("java")) {
					compiler.compile(new JavaLanguage(xpd.getProtocol(), clo.getOutputDirectory(), clo.getParams("java")));
				} else {
					Class<?> clazz = null;
					CompilerLanguage lang = null;
					try {
						clazz = Class.forName(string);
						if (clazz.isAssignableFrom(CompilerLanguage.class)) {
							lang = (CompilerLanguage) clazz.getConstructor(Protocol.class, File.class, Map.class).newInstance(xpd.getProtocol(), clo.getOutputDirectory(), clo.getParams(clazz.getCanonicalName()));
						}
						compiler.compile(lang);
					} catch (ClassNotFoundException e) {
						logger.error(e.getLocalizedMessage(), e);
					} catch (InstantiationException e) {
						logger.error(e.getLocalizedMessage(), e);
					} catch (IllegalAccessException e) {
						logger.error(e.getLocalizedMessage(), e);
					} catch (IllegalArgumentException e) {
						logger.error(e.getLocalizedMessage(), e);
					} catch (InvocationTargetException e) {
						logger.error(e.getLocalizedMessage(), e);
					} catch (NoSuchMethodException e) {
						logger.error(e.getLocalizedMessage(), e);
					} catch (SecurityException e) {
						logger.error(e.getLocalizedMessage(), e);
					}
					continue;
				}
			}
		} catch (SAXException | IOException | ParserConfigurationException e) {
			logger.error(e.getLocalizedMessage(), e);
		}

	}
}
