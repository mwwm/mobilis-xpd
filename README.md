#Mobilis XPD&XSD Compiler
The way that protocols and data models in Mobilis are defined has changed. The old MSDL has been split into two parts:

* XPD - XMPP Protocol Description (protocol, old: interface, binding and service)
* XSD - XML Schema Definition (data model, old: types)

## Usage
If you're a proud owner of a prebuilt *.jar file you can invoke the compiler with the following options on the command line:

### Required parameters

Required parameters are always necessary.

#### Protocol Description

    -xpd, --protocol-description
    XPD Protocol Description

Path to the *.xpd file.

#### Data Model

    -xsd, --data-scheme
    XSD Protocol Data Scheme

Path to the *.xsd file. Attention: We do only support a very restricted schema at this type. Please have a look at "XPD & XSD Format & Restrictions" later in this document.

    
### Optional parameters

Optional parameters can be omitted but they are useful sometimes.

#### Languages

    -l, --languages
	Languages that will be generated. Pass full qualified class names that are available on class path to invoke your custom scripts.
    Default: [java, objc]

You can specify the languages for which code will be generated. Languages built into the compiler are currently:

* Java
* Objective-C

You can extend the compiler to fit your needs by subclassing `CompilerLanguage`. You can find details on that later in this document.

#### Output Directory
You can specify an output directory. By default a directory called `out` will be created in the current working directory.

    -o, --output-directory
    Folder, where generated files will be stored. Additional structure is generated language dependent.
    Default: ./out
    
#### Language-specific options
You can specify language-specific options via `-D`. To change the generated package namespace for generated Java code you can pass it via `-Djava:packageNamespace=your.name.space`. These options are currently not shown in the hep section.

    -D
    Provide additional parameters (like package names, etc.) via 
    -D<lang|FQCN>:key=value. These options will be passed to the corresponding compiler language!
    Syntax: -Dkey=value
    Default: {}
    
### Example(s)
This reads the necessary files for the NineCards demo project and creates only java code and writes it into a specific directory.

	-xsd 9Cards.xsd -xpd 9Cards.xpd -l java -o /Developer/workspaces/java/9cards/MobilisNineCards_Service/src
    
    
## XPD & XSD Format & Restrictions
The current version of this tool limits support for XML schema. We only provide a small subset of XML schema at this moment. You'll find further details in this section.

### XPD Format

The new XMPP Protocol Description is XML based and consists of a `protocol` and multiple `operation` parts.

#### XPD Protocol part
The protocol part defines the service name and namespace as well as version, roles and channels that are used by roles to communicate (there are no channels for direct communication). At this moment only MUC Channels are available. Also the service type (single or multi instance) is defined here.

#### XPD Operations
Operations can be defined as request/response (IQ based) or as messages without responses (Message based). Presence Stanzas are not supported yet.

You must specify directions for each operation. Therefore you've defined roles in the protocol part. These directions are used to create role-specific code stubs and interfaces.

You can also add a `via` attribute which receives a channel defined in the protocol part. Code templates and language generators may use these information to improve generated code.

### XSD Restrictions
As already mentioned we do not have full XML schema support yet.

#### Schema Usage Guide
It's rather simple to write your schema. Use `xs:element` for all Beans (IQs, Messages) and `xs:complexType` to define nested complex objects.

#### Restrictions
If it's necessary that you have nested objects in your application, you must use `xs:complexType` instead of nesting `xs:complexType`s recursively. This would create anonymous inner types (or classes) which are difficult to handle and aren't reusable. Maybe this will be supported later.

#### Examples
Bean without attributes.

	<xs:element name="GetGameConfigurationRequest" />

Bean with multiple scalar attributes.

	<xs:element name="GetGameConfigurationResponse">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="muc" type="xs:string" />
				<xs:element name="maxRounds" type="xs:int" />
				<xs:element name="maxPlayers" type="xs:int" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>

Bean with multiple attributes. Also supported: lists (`minOccurs` and `maxOccurs`)and nested objects (`PlayerInfo`) and combination of both.

	<xs:element name="GameOverMessage">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="winner" type="xs:string" />
				<xs:element name="score" type="xs:int" />
				<xs:element name="playerInfos" type="PlayerInfo"
					minOccurs="0" maxOccurs="unbounded" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>

To use this example you have to define `PlayerInfo` as `xs:complexType`.

	<xs:complexType name="PlayerInfo">
		<xs:sequence>
			<xs:element name="id" type="xs:string" />
			<xs:element name="score" type="xs:int" />
			<xs:element name="usedcards" type="xs:int" minOccurs="0"
				maxOccurs="unbounded" />
		</xs:sequence>
	</xs:complexType>
	
You can find a full example in the NineCards repository (usually these files are located in the root or service directory).

## Build from scratch.

If you do not have a runnable jar file you can build it on your own or run the compiler from eclipse.

### Maven Build

1. Check out sources.
2. Run `mvn package` (at least) or `mvn install`.
3. You'll find `xpd-compiler.jar` in `target` directory.

### Use from eclipse

We recommend not to use eclipse integrated Maven support of eclipse 4.x. 

1. Check out sources.
2. Run `mvn eclipse:eclipse`.
3. Open eclipse / IntelliJ and import project.

Further details are available [here](http://maven.apache.org/plugins/maven-eclipse-plugin/).

## Create your own Language

To provide resources for another language than Java or Objective-C you'll need to:

* subclass `CompilerLanguage.java` and implement at least all abstract methods
* provide templates where necessary

### Subclass `CompilerLanguage.java`
You should have a look at `CompilerLanguage.java` itself, it contains lot of javadoc. Basically there are three kinds of methods:

* `get...Templates` should return a `List<String>` of template file names. Avoid using paths, you can manage paths by overriding `getTemplatePrefix`. The return value of `getTemplatePrefix` is passed as second parameter to [ClassTemplateLoader.ClassTemplateLoader(Class, String)](http://freemarker.org/docs/api/freemarker/cache/ClassTemplateLoader.html#ClassTemplateLoader%28java.lang.Class, java.lang.String%29). See Java or Objective-C implementation for examples.
* `getWriter...` returns a `Writer` which enables File output. We decided to choose only `Writer` to enable also single-file output for languages like JavaScript. Maybe this needs to be enhanced in upcoming versions. If you do not implement this everything will be redirected to `stdout`.
* `getData...` should return additional data for your templates. Check the examples and the freemarker documentation for further details.

### Provide templates
Basically you can copy existing templates to your project and adjust them. For details about freemarker check their [documentation](http://freemarker.org/docs/index.html).

### Usage

We encourage you to check out / fork our project, implement your own language and create a pull request so that we can provide languages to other users. Therefore you simply have to adjust `XpdCompiler.java` to fit your needs (e.g. additional command line parameters and abbreviations for language classes to use in `-l` option).

Alternatively you can place your classes and templates on the class path of the jar file and use dynamic class loading with `-l full.qualified.class.Name`.